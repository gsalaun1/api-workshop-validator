[![pipeline status](https://gitlab.com/gsalaun1/api-workshop-validator/badges/main/pipeline.svg)](https://gitlab.com/gsalaun1/api-workshop-validator/-/commits/main)

# Api Workshop Validtor

Exercide de recrutement candidat.

Validateur frontend du code produit par le candidat.

# OBJECTIF

Le but de ce workshop est de constuire une API de manipulation d'une liste de langage de programmation.

Caractéristiques d'un langage de programmation : 
- Id : number
- Typing (deux possibilités uniquement) : dynamic, static
- Name : string
- Year : number

# PREREQUIS

- yarn

# Lancer l'application

Installer les dépendances : ```yarn install```

Lancer l'application : ```yarn start```

# Déploiement de l'application

L'application est disponible ici : https://gsalaun1.gitlab.io/api-workshop-validator