export default interface IExpected<T> {
  code: number;
  content: T | T[];
}
