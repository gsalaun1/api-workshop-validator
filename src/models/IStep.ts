import IExpected from "./IExpected";
import IOptions from "./IOptions";

export default interface IStep<T> {
  path: string;
  options: IOptions<T>;
  expected: IExpected<T>;
}
