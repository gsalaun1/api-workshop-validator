export default interface Language {
  id: number;
  typing: TYPING;
  name: string;
  year: number;
}

enum TYPING {
  CLASSIC,
  DYNAMIC
}
