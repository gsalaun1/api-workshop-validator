export default interface IOptions<T> {
  body?: T;
  method: "GET" | "POST" | "PUT" | "DELETE";
}
