import { LOCAL_STORAGE_APP_CONTEXT } from "config/constants";
import AppProperties, { getDefaultProperties } from "domain/AppProperties";
import React, { createContext, useEffect, useReducer } from "react";

const AppContext = createContext<{
  appProperties: AppProperties;
  setAppProperties: (value: AppProperties) => void;
}>({
  appProperties: getDefaultProperties(),
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  setAppProperties: (_: AppProperties) => null
});

const reducer = (appProperties: any, newAppProperties: any) => {
  if (newAppProperties === null) {
    localStorage.removeItem(LOCAL_STORAGE_APP_CONTEXT);
    return initialState;
  }
  return { ...appProperties, ...newAppProperties } as AppProperties;
};

const initialState = getDefaultProperties();

const sessionState = () => {
  const storedAppContext = localStorage.getItem(LOCAL_STORAGE_APP_CONTEXT);
  if (storedAppContext === null) {
    return null;
  }
  return JSON.parse(storedAppContext) as AppProperties;
};

const AppProvider = (props: any) => {
  const [appProperties, setAppProperties] = useReducer(
    reducer,
    sessionState() || initialState
  );

  useEffect(() => {
    localStorage.setItem(
      LOCAL_STORAGE_APP_CONTEXT,
      JSON.stringify(appProperties)
    );
  }, [appProperties]);

  return (
    <AppContext.Provider value={{ appProperties, setAppProperties }}>
      {props.children}
    </AppContext.Provider>
  );
};

export { AppContext, AppProvider };
