import Steps from "components/Steps";
import WorkshopConfiguration from "components/WorkshopConfiguration";
import React from "react";

import { useStyles } from "./styles";
import { Grid } from "@material-ui/core";

const Home: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.home}>
      <Grid container justifyContent="center">
        <Grid item xs={12} sm={10} lg={8}>
          <WorkshopConfiguration />
          <Steps />
        </Grid>
      </Grid>
    </div>
  );
};

export default Home;
