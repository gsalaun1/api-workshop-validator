import { AppBar, Toolbar, Typography } from "@material-ui/core";
import React from "react";

import { useStyles } from "./styles";

const Header: React.FC = () => {
  const classes = useStyles();

  return (
    <AppBar position="static" className={classes.header}>
      <Toolbar>
        <Typography variant="h6">Api Workshop Validator</Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
