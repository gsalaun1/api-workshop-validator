import useFetch from "hooks/useFetch";
import ILanguage from "models/ILanguage";
import IExpected from "models/IExpected";
import IStep from "models/IStep";
import React, { useEffect, useState } from "react";
import ReactJson from "react-json-view";

import {
  Chip,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  LinearProgress,
  Typography
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ErrorIcon from "@material-ui/icons/Error";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ReplayIcon from "@material-ui/icons/Replay";

import useStyles from "./styles";

interface StepProps {
  step: IStep<ILanguage>;
  setStepsOk: (fun: (stepsOk: number[]) => number[]) => void;
  index: number;
  currentStep: number | null;
}

interface ResponseProps {
  title: string;
  response: IExpected<ILanguage> | null;
}

const Response: React.FC<ResponseProps> = ({ title, response }) => {
  const classes = useStyles();
  if (response !== null) {
    const content = JSON.stringify(response.content);
    return (
      <>
        <Typography align="center" className={classes.typoData}>
          {title}{" "}
          <Chip
            label={`Code: ${response.code}`}
            component={"span"}
            size="small"
          />
        </Typography>
        <div className={classes.reactJson}>
          {
            <ReactJson
              src={JSON.parse(content)}
              theme="google"
              name={false}
              displayDataTypes={false}
              displayObjectSize={false}
              enableClipboard={false}
            />
          }
        </div>
      </>
    );
  }
  return null;
};

const Step: React.FC<StepProps> = ({
  step: { path, options, expected },
  setStepsOk,
  index,
  currentStep
}) => {
  const [shouldLaunchRequest, setShouldLaunchRequest] = useState(false);
  const { isLoading, response, isError } = useFetch(
    path,
    options,
    shouldLaunchRequest,
    expected
  );
  const [shouldGoToNextStep, setShouldGoToNextStep] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    if (currentStep === index + 1) {
      setShouldLaunchRequest(true);
    }
  }, [index, currentStep]);

  useEffect(() => {
    if (
      !isError &&
      !isLoading &&
      response !== null &&
      currentStep !== null &&
      !shouldGoToNextStep
    ) {
      setShouldGoToNextStep(true);
      setStepsOk((stepsOk: number[]) => {
        const array = [...stepsOk, currentStep];
        return array.filter(
          (stepOk, arrIndex) => array.indexOf(stepOk) === arrIndex
        );
      });
    }
    // eslint-disable-next-line
  }, [response, isLoading, isError, currentStep, shouldGoToNextStep]);

  useEffect(() => {
    if (response !== null || isError) {
      setShouldLaunchRequest(false);
    }
  }, [response, isError]);

  const handleReplay = () => {
    setShouldLaunchRequest(true);
  };

  if (!shouldLaunchRequest && !isError && response === null) {
    return null;
  }

  return (
    <ExpansionPanel>
      <div>
        {isLoading && <LinearProgress />}
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            content: classes.spaceBetween
          }}
        >
          <Typography
            classes={{ body1: classes.body1 }}
            className={classes.expansionTitle}
          >
            {options.method}
          </Typography>
          <div className={classes.flexDisplay}>
            <Typography
              className={classes.marginRight}
              classes={{ body1: classes.body1 }}
            >
              {path}
            </Typography>
            {isError ? (
              <Chip
                icon={<ErrorIcon />}
                label="Error"
                color="secondary"
                onDelete={handleReplay}
                deleteIcon={<ReplayIcon />}
              />
            ) : (
              !isLoading && (
                <Chip icon={<CheckIcon />} label="Success" color="primary" />
              )
            )}
          </div>
        </ExpansionPanelSummary>
      </div>
      <ExpansionPanelDetails>
        {options.method === "POST" || options.method === "PUT" ? (
          <div className={classes.flexDisplayWidth100}>
            <div style={{ flexGrow: 1, margin: "0px 5px" }}>
              <Typography align="center" className={classes.typoData}>
                Données envoyées
              </Typography>
              <div className={classes.reactJson}>
                {options.body && <ReactJson
                  src={options.body}
                  theme="google"
                  name={false}
                  displayDataTypes={false}
                  displayObjectSize={false}
                  enableClipboard={false}
                />}
              </div>
            </div>
            {isError && (
              <div style={{ flexGrow: 1, margin: "0px 5px" }}>
                <Response title="Réponse attendue" response={expected} />
              </div>
            )}
            <div style={{ flexGrow: 1, margin: "0px 5px" }}>
              <Response title="Réponse reçue" response={response} />
            </div>
          </div>
        ) : (
          <div className={classes.flexDisplayWidth100}>
            {isError && (
              <div style={{ flexGrow: 1, margin: "0px 5px" }}>
                <Response title="Réponse attendue" response={expected} />
              </div>
            )}
            <div style={{ flexGrow: 1, margin: "0px 5px" }}>
              <Response title="Réponse reçue" response={response} />
            </div>
          </div>
        )}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export default Step;
