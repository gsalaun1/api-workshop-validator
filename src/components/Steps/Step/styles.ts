import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

export default makeStyles((theme: Theme) =>
  createStyles({
    body1: {
      lineHeight: 2
    },
    expansionTitle: {
      fontWeight: "bold"
    },
    flexDisplay: {
      display: "flex"
    },
    flexDisplayWidth100: {
      display: "flex",
      width: "100%",
      justifyContent: "space-between"
    },
    marginRight: {
      marginRight: theme.spacing()
    },
    spaceBetween: {
      justifyContent: "space-between"
    },
    typoData: {
      fontWeight: "bold",
      borderBottom: "1px solid lightgrey",
      paddingBottom: theme.spacing(),
      marginBottom: theme.spacing()
    },
    reactJson: {
      width: "100%",
      maxHeight: "400px",
      overflowY: "auto",
      borderRadius: "3px"
    },
    width49: {
      width: "49%"
    },
    width100: {
      width: "100%"
    }
  })
);
