import React from "react";
import { Button } from "@material-ui/core";

interface LaunchStepsProps {
  setShouldLaunchSteps: (value: boolean) => void;
}

const LaunchSteps: React.FC<LaunchStepsProps> = ({ setShouldLaunchSteps }) => {
  return (
    <Button
      variant="contained"
      color="primary"
      onClick={() => {
        setShouldLaunchSteps(true);
      }}
      fullWidth
    >
      Démarrer
    </Button>
  );
};

export default LaunchSteps;
