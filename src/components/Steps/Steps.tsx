import React, { useState, useEffect } from "react";
import LaunchSteps from "./LaunchSteps";
import Step from "./Step";
import steps from "steps/steps.json";
import IStep from "models/IStep";
import IEtablissement from "models/ILanguage";
import useStyles from "./styles";
const Steps: React.FC = () => {
  const classes = useStyles();
  const [shouldLaunchSteps, setShouldLaunchSteps] = useState(false);
  const [stepsToExecute, setStepsToExecute] = useState<number[]>(
    Array.from(steps, (_, index: number) => index + 1)
  );
  const [stepsOk, setStepsOk] = useState<number[]>([]);
  const [currentStep, setCurrentStep] = useState<number | null>(null);

  useEffect(() => {
    if (shouldLaunchSteps && stepsToExecute.length > 0) {
      setCurrentStep(stepsToExecute[0]);
    }
  }, [shouldLaunchSteps, stepsToExecute]);

  useEffect(() => {
    if (stepsOk.length > 0) {
      setStepsToExecute((stepsToExecuteTmp: number[]) =>
        [...stepsToExecuteTmp].filter((step: number) => !stepsOk.includes(step))
      );
    }
  }, [stepsOk]);

  return (
    <>
      <div className={classes.lauchButton}>
        <LaunchSteps setShouldLaunchSteps={setShouldLaunchSteps} />
      </div>
      {shouldLaunchSteps &&
        ((steps as unknown) as IStep<
          IEtablissement
        >[]).map((step: IStep<IEtablissement>, index: number) => (
          <Step
            key={index}
            step={step}
            setStepsOk={setStepsOk}
            index={index}
            currentStep={currentStep}
          />
        ))}
    </>
  );
};

export default Steps;
