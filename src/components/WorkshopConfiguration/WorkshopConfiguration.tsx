import {
  Button,
  Grid,
  TextField,
  InputAdornment,
  IconButton,
  Typography
} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import { AppContext } from "context/AppContext";
import React, { useContext, useEffect, useState } from "react";

import { useStyles } from "./styles";

const WorkshopConfiguration: React.FC = () => {
  const classes = useStyles();

  const [apiUrl, setApiUrl] = useState("");

  const [editUrl, setEditUrl] = useState(true);

  const { appProperties, setAppProperties } = useContext(AppContext);

  useEffect(() => {
    if (appProperties.apiUrl === null) {
      setEditUrl(true);
    } else {
      setApiUrl(appProperties.apiUrl);
      setEditUrl(false);
    }
  }, [appProperties]);

  const updateUrl = (event: any) => {
    setApiUrl(event.target.value);
  };

  const saveUrl = () => {
    const splittedUrl = apiUrl.split("http://");
    const url = splittedUrl.length > 1 ? splittedUrl[1] : splittedUrl[0];
    setAppProperties({ ...appProperties, apiUrl: `http://${url}` });
    setEditUrl(false);
  };

  const switchToUrlEdition = () => {
    setEditUrl(true);
  };

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter") {
      saveUrl();
    }
  };

  if (editUrl) {
    return (
      <Grid container>
        <Grid item xs={4} className={classes.item}>
          <TextField
            label="Adresse de votre serveur"
            fullWidth
            onKeyDown={handleKeyDown}
            onChange={updateUrl}
            value={apiUrl.split("http://")[1]}
            autoFocus
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">http://</InputAdornment>
              )
            }}
          />
        </Grid>
        <Grid item xs={2} className={classes.item}>
          <Button variant="contained" color="primary" onClick={saveUrl}>
            Valider
          </Button>
        </Grid>
      </Grid>
    );
  } else {
    return (
      <>
        <Typography style={{ fontWeight: "bold" }}>Serveur : </Typography>
        {apiUrl}
        <IconButton
          onClick={switchToUrlEdition}
          size="small"
          className={classes.button}
        >
          <EditIcon />
        </IconButton>
      </>
    );
  }
};

export default WorkshopConfiguration;
