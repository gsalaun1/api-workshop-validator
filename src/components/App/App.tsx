import { createTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import Header from "components/Header";
import Home from "components/Home";
import React, { useState } from "react";

import styles from "./App.module.scss";

const palette = {
  primary: {
    main: styles["primary-main"]
  },
  secondary: {
    main: styles["secondary-main"]
  }
};

const defaultTheme = createTheme({
  palette
});

const App: React.FC = () => {
  const [theme] = useState(defaultTheme);

  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Home />
    </ThemeProvider>
  );
};

export default App;
