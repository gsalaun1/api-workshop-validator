import { AppProvider } from "context/AppContext";
import React from "react";
import App from "./App";

const AppContainer: React.FC = () => {
  return (
    <AppProvider>
      <App />
    </AppProvider>
  );
};

export default AppContainer;
