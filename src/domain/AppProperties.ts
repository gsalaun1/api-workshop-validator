export default interface AppProperties {
  apiUrl: string | null;
}

export const getDefaultProperties = (): AppProperties => ({
  apiUrl: null
});
