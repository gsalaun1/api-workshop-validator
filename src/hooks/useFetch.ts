import { AppContext } from "context/AppContext";
import _ from "lodash";
import IExpected from "models/IExpected";
import IOptions from "models/IOptions";
import { useContext, useEffect, useState } from "react";

interface UseFetchHook<T> {
  response: IExpected<T> | null;
  isError: boolean;
  isLoading: boolean;
}

function useFetch<T>(
  url: string,
  options: IOptions<T>,
  shouldLaunchRequest: boolean,
  expected: IExpected<T>
): UseFetchHook<T> {
  const { appProperties } = useContext(AppContext);
  const [response, setResponse] = useState<IExpected<T> | null>(null);
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setIsError(false);
      setResponse(null);
      try {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        const res = await fetch(`${appProperties.apiUrl}${url}`, {
          method: options.method,
          body: JSON.stringify(options.body),
          headers
        });
        if (res.status !== expected.code) {
          setIsError(true);
        }

        const json = await res.json().catch((error: Error) => {
          if (
            res.status === 204 &&
              (error.message.indexOf(
              "Unexpected end of JSON input"
            ) > -1
                  || error.message.indexOf(
                  "unexpected end of data at line 1 column 1 of the JSON data"
              ) > -1)
          ) {
            return {};
          }
          throw error;
        });
        const receivedData = {
          code: res.status,
          content: json
        };
        setResponse(receivedData);
        if (!_.isEqual(json, expected.content)) {
          setIsError(true);
        }
        setIsLoading(false);
      } catch (error : any) {
        if (error.status !== expected.code) {
          setIsError(true);
          const receivedData = {
            code: error.status,
            content: error
          };
          setResponse(receivedData);
        }
        setIsLoading(false);
      }
    };
    if (shouldLaunchRequest) {
      fetchData();
    }
    // eslint-disable-next-line
  }, [shouldLaunchRequest]);

  return { response, isError, isLoading };
}

export default useFetch;
